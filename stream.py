import pyaudio
import queue

class microphone(object):
	def __init__(self, sample_rate, chunk):
		self.buffer = queue.Queue()
		self.pa = pyaudio.PyAudio()
		self.stream = self.pa.open(channels=1, format=pyaudio.paInt16, rate=sample_rate, frames_per_buffer=chunk, input=True, stream_callback=self.callback)
	
	def callback(self, in_data, frame_count, time_info, status):
		self.buffer.put(in_data)
		return (None, pyaudio.paContinue)
		
	def read(self):
		return self.buffer.get()

	def __del__(self):
		self.stream.stop_stream()
		self.stream.close()
		self.pa.terminate()

